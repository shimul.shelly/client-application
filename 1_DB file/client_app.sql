-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 07, 2018 at 02:45 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.28-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `client_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_token`
--

CREATE TABLE `access_token` (
  `id` int(11) NOT NULL,
  `token` varchar(2048) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `access_token`
--

INSERT INTO `access_token` (`id`, `token`) VALUES
(1, 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRjNTlhZjQ2MTc2MDFkYjRmYWNlM2JiOTYxNmZhMGU2ZTY1NTkwZjI3NDI5YzJiODBiN2M3N2JhNjkzYWY1ZmI1YTFhYWU0MmFjYzQ5YzRjIn0.eyJhdWQiOiIzIiwianRpIjoiZGM1OWFmNDYxNzYwMWRiNGZhY2UzYmI5NjE2ZmEwZTZlNjU1OTBmMjc0MjljMmI4MGI3Yzc3YmE2OTNhZjVmYjVhMWFhZTQyYWNjNDljNGMiLCJpYXQiOjE1MjU3MDIxODcsIm5iZiI6MTUyNTcwMjE4NywiZXhwIjoxNTU3MjM4MTg3LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.Yl0lb_okK4B_Rva-7iERPPMCcQhIP4ZyRysM1JERiudKIlHii4SAOLVO6vPOpNt06EHEJAgVjNH-2PYlQhz3k1OOykzGqsmADULn62-QfWU8tXH7pjM3ja-kAGa8NNKScz4LCzQdPnOCfsBggZYo1RZvr17ZMI11V45JcVj0i8tFqxDHiGcKPnfFbJ1_iIFpjcNH53UFRpBwR0gb20zFPYGLf3TKOMVYsdSpPASD0oWNTvMWWusvvHTvnyoYlsANpY8qHxhenpP9sOSfitGJTnnvlElXXE-XJQwHt0pPjtYUf5S161IzMhFMnI5RMi0fxgbGbQpntbLPTMKHi8dNG0HztU9x3HUMQkZO4rENjlDsAjcd8gTncv-i3z9J2frPI-rdTbvvfDWbs_yINdAZn95zZbOepoLgm9iUMZHpbyQCnFIVS6Ptqb1__oLs6dyADnrSxo0U2dQg2bCXyXBE4aDUoYV65LKoy3xbXoor01K3IFYcGAU1PAXMLMPKgYnKh_9QuB90J-16OGZTOHD4ybDeiwq6sHCtxQHieQjY7FSMEAWYru88tzPhFvE3Xf6H-8ZLOPzTfAyxsPeQkPxAYcPWlVm6pPgyMjF5LXnQ_GBce1hWxo3-O7B8dukTLnMpDYMfVsqoCOpw2vtBqT-m5AT-kcj-p63b0jcp-MBRB44');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
