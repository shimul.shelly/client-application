<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});


Route::get('/redirect', function () {
    
    $query = http_build_query([
        'client_id' => '3',
        'redirect_uri' => 'http://client-app.dev/callback',
        'response_type' => 'code',
        'scope' => '',
    ]);
    
    return redirect('http://resource-server.dev/oauth/authorize?'.$query);
    
})->name('apptoken');


Route::get('/callback', function (Request $request) {
    $http = new GuzzleHttp\Client;
    
    $response = $http->post('http://resource-server.dev/oauth/token', [
        'form_params' => [
            'grant_type' => 'authorization_code',
            'client_id' => '3',
            'client_secret' => 'KBfBSkBUo8PyAlWnNJqDfyokz2MXy31QSXtjyC8r',
            'redirect_uri' => 'http://client-app.dev/callback',
            'code' => $request->code,
        ],
    ]);
    
    
    $result = json_decode($response->getBody()->getContents());
    
/*     var_dump($result);
    
    var_dump($result->access_token); */
    
    DB::table('access_token')
                ->where('id', 1)
                ->update(['token' => $result->access_token]);
    
    return json_decode((string) $response->getBody(), true);
});


Route::get('/show-user-data', function (){
    
    $users = DB::table('access_token')->select('token')->get();
    
    $curl = curl_init();
    
    curl_setopt_array($curl, array(
        CURLOPT_URL => "http://resource-server.dev/api/info",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Accept: application/json",
            "Authorization: Bearer ".$users[0]->token,
            "Cache-Control: no-cache",
            "Content-Type: application/json"
        ),
    ));
    
    $response = curl_exec($curl);
    $err = curl_error($curl);
    
    curl_close($curl);
    
    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        echo $response;
    }
        
})->name('show_data');